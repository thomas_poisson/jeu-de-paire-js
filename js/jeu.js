//créer variable

var nb_clics = 0; //nb de clics à 0 de base
var mini1 = "";
var mini2 = "";
var card1 = "";
var card2 = "";
var img_ok = 0;
var nb_erreurs = 0; //nb d'erreurs à 0 de base
var le_score = 0;
var depart = false;
var temps_debut = new Date().getTime();

//generation des cartes

generation();

//2 sec d'attente avant apparition des cartes
var attente = setTimeout(function () {
  for (var i = 0; i < 12; i++) {
    //boucle pour définir le nb de cartes + incrémentation (i++)
    document.getElementById("img" + i).src = "mini/miniz.png"; //récupération de l'id img + i.src pour définir la source
  }
  depart = true;
}, 2000); //nb de millisecondes avant le début de la partie

function startTimer(duration, display) {
  let timer = duration,
    minutes,
    seconds;

  let timerInterval = setInterval(function () {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    if (timer <= 0) {
      clearInterval(timerInterval);
      alert("Temps Ecoulé, vous avez perdu !");
      window.location.reload();
    }

    display.textContent = minutes + ":" + seconds;

    if (--timer < 0) {
      timer = duration;
    }
  }, 1000);
}

window.onload = function () {
  let threeMinutes = 60 * 2,
    display = document.querySelector("#timer");
  startTimer(threeMinutes, display);
};

function generation() {
  var nb_alea;
  var nb_img = "";
  var test = true;
  var chaine = "";

  //boucle avec la condition des 12 cartes
  for (var i = 0; i < 12; i++) {
    while (test == true) {
      //pendant que test = true
      //positionne les cartes aléatoirement
      nb_alea = Math.floor(Math.random() * 12) + 1;

      if (chaine.indexOf("-" + nb_alea + "-") > -1)
        nb_alea = Math.floor(Math.random() * 12) + 1;
      else {
        nb_img = Math.floor((nb_alea + 1) / 2);
        document.getElementById("case" + i).innerHTML =
          "<img style='cursor:pointer;' id='img" +
          i +
          "' src='mini/mini" +
          nb_img +
          ".png' onClick='verify(\"img" +
          i +
          '","mini' +
          nb_img +
          "\")' alt='' />";
        chaine += "-" + nb_alea + "-";
        test = false;
      }
    }
    test = true;
  }
}

function verify(limg, source) {
  if (depart == true) {
    nb_clics++;
    document.getElementById(limg).src = "mini/" + source + ".png";
    if (nb_clics == 1) {
      mini1 = source;
      card1 = limg;
    } else {
      mini2 = source;
      card2 = limg;

      if (card1 != card2) {
        depart = false;
        if (mini1 != mini2) {
          var attente = setTimeout(function () {
            document.getElementById(card1).src = "mini/miniz.png";
            document.getElementById(card2).src = "mini/miniz.png";
            depart = true;
            nb_clics = 0;
            nb_erreurs++;
            if (nb_erreurs == 10) {
              alert("Vous avez perdu !");
              window.location.reload();
            }

            if (nb_erreurs < 11) le_score = 10 - nb_erreurs;
            document.getElementById("score").innerHTML =
              "<strong>" + le_score + "</strong>/10";
          }, 1000);
        } else {
          depart = true;
          nb_clics = 0;
          img_ok += 2;
          if (img_ok == 12) {
            var dif_temps = Math.floor(
              (new Date().getTime() - temps_debut) / 1000
            );
            document.getElementById("score").innerHTML = le_score + "/ 10";
            document.getElementById("temps").innerHTML =
              "Vous avez mis " + dif_temps + " secondes";
          }
          if (dif_temps > 180) {
            document.getElementById("temps").innerHTML = "T'as perdu!";
            depart = false;
          }
        }
      } else {
        if (nb_clics == 2) nb_clics = 1;
      }
    }
  }
}
